import React, { useState, useEffect } from 'react'
import { DescriptionDetail } from './description-detail/DescriptionDetail';
import './styles.css';

// Images
import editorDesktop from '../../../assets/illustration-editor-desktop.svg';
import editorMobile from '../../../assets/illustration-editor-mobile.svg';

export const Description = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getData()
    })

    const getData = async () => {
        const url = 'https://posts-test-sting.vercel.app/api';
        // await fetch(url).then(resp => resp.json()).then(data => console.log(data));
        const resp = await fetch(url);
        const { data } = await resp.json();

        const posts = data.map(post => {
            return {
                id: post.id,
                title: post.title,
                body: post.body,
            }
        })

        setPosts(posts);
    }

    const getPost = (post) => {
        for (let i = 0; i <= post.length - 1; i++) {
            return <DescriptionDetail key={post.id} {...post} />
        }
    }

    return (
        <>
            <section className="description">
                <h2>Designed for the future</h2>
                <div className="description_content">
                    {
                        getPost(posts)
                    }
                    <div className="description_content_rightcolumn">
                        <img src={editorDesktop} alt="editor illustration" className="desktop_editor" />
                        <img src={editorMobile} alt="editor illustration" className="mobile_editor" />
                    </div>
                </div>
            </section>
        </>
    )
}
