import React, { useState, useEffect } from 'react'
import { FeatureDetail } from './feature-detail/FeatureDetail';
import './styles.css';

// Images
import laptopDesktop from '../../../assets/illustration-laptop-desktop.svg';
import laptopMobile from '../../../assets/illustration-laptop-mobile.svg';

export const Features = () => {

    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getData()
    })

    const getData = async () => {
        const url = 'https://posts-test-sting.vercel.app/api';
        // await fetch(url).then(resp => resp.json()).then(data => console.log(data));
        const resp = await fetch(url);
        const { data } = await resp.json();

        const posts = data.map(post => {
            return {
                id: post.id,
                title: post.title,
                body: post.body,
            }
        })

        setPosts(posts);
    }

    const getPost = (post) => {
        for (let i = 0; i <= post.length - 1; i++) {
            return <FeatureDetail key={post.id} {...post} />
        }
    }

    return (
        <>
            <section className="features">
                <div className="features_leftcolumn">
                    <img src={laptopDesktop} alt="laptop illustration" className="desktop_illustration" />
                    <img src={laptopMobile} alt="laptop illustration" className="mobile_illustration" />
                </div>
                {
                    getPost(posts)
                }
            </section>
        </>
    )
}
