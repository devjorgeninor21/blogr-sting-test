import React from 'react'
import { Content } from '../content/Content'
import { Footer } from '../footer/Footer'
import { Header } from '../header/Header'

export const Blogr = () => {

    return (
        <>
            <Header />
            <Content />
            <Footer />
        </>
    )
}

