/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import './styles.css';
import { HeaderMobile } from './header-mobile/HeaderMobile';

// Images
import Logo from '../../assets/logo.svg';
import iconArrow from '../../assets/icon-arrow-light.svg';
import iconHamburger from '../../assets/icon-hamburger.svg';

export const Header = () => {

    return (
        <>
            <section className="header">
                <div className="navigation">
                    <a href="#"><img src={Logo} alt="Blogr logo" /></a>
                    <ul className="site_nav">
                        <li>
                            <a href="#">Product</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                            <div className="dropdown_menu">
                                <ul>
                                    <li><a href="#">Overview</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Marketplace</a></li>
                                    <li><a href="#">Features</a></li>
                                    <li><a href="#">Integrations</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="#">Company</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                            <div className="dropdown_menu">
                                <ul>
                                    <li><a href="#">About</a></li>
                                    <li><a href="#">Team</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Careers</a></li>
                                </ul>
                            </div>
                        </li>

                        <li>
                            <a href="#">Connect</a>
                            <img src={iconArrow} alt="dropdown arrow" />
                            <div className="dropdown_menu">
                                <ul>
                                    <li><a href="#">Contact</a></li>
                                    <li><a href="#">Newsletter</a></li>
                                    <li><a href="#">LinkedIn</a></li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                    <ul className="register">
                        <li><a href="#" className="login">Login</a></li>
                        <li><a href="#" className="signup">Sign Up</a></li>
                    </ul>
                    <img src={iconHamburger} alt="hamburger" id="mobilemenu" />
                </div>

                <div className="header_content">
                    <h1>A modern publishing platform</h1>
                    <p>Grow your audience and build your online brand</p>
                    <ul className="header_content_links">
                        <li><a href="#" className="start">Start for free</a></li>
                        <li><a href="#" className="learn">Learn More</a></li>
                    </ul>
                </div>
            </section>

            <HeaderMobile />

        </>
    )


}

